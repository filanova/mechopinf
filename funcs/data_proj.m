function [ xp, vp, ap, varargout ] = data_proj(Vr,x,v,a,varargin)
% DATA_PROJ Data projection for operator inference method
%   This function provides the projected data for operator inference method
%   using POD basis.
% Input: 
%       [x]  : state trajectory matrix
%       [v]  : velocity matrix
%       [a]  : acceleration matrix
%       [Vr] : projection basis
% varargin{1} : external force matrix
%
% Out:
%       [xp]  : projected state trajectory matrix
%       [vp]  : projected velocity matrix
%       [ap]  : projected acceleration matrix
% varargout{1} : projected external force matrix
% ========================================================================

xp = Vr'*x;
vp = Vr'*v;
ap = Vr'*a; 
if nargin == 5
    F = varargin{1};
    varargout{1} = Vr'*F; 
end
end

