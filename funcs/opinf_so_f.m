 function [opinf2s] = opinf_so_f(x, y, r, Vr, v, a, F, lambda)
% OPINF2C Operator Inference reduction for second-order systems with known
% external force vector
% Input: 
%       [x]  : state trajectory matrix
%       [v]  : velocity matrix
%       [a]  : acceleration matrix
%       [y]  : output trajectory
%       [Vr] : projection basis
%   lambda   : regularization parameter
%
% Out:
%     {opinf2s} - structured array with fields:
%                 [opinf2s.M]  :  mass matrix
%                 [opinf2s.K]  :  stiffness matrix
%                 [opinf2s.D]  :  damping matrix
%                 [opinf2s.B]  :  input matrix
%                 [opinf2s.Cd] :  position output matrix
%                 [opinf2s.Cv] :  velocity output matrix
% ========================================================================

% Data projection
    [ xp, vp, ap, fp ] = data_proj(Vr,x,v,a,F);
    f_pod = fp';
% Solving LMI-constrained optimization
    opinf2s   = opinf_lsqlmi(r, ap, vp, xp, f_pod, lambda );
    opinf2s.F = fp;
    
% Output-operator
if ~isempty(y)
    C = lsqminnorm([xp', vp'],y');
    opinf2s.C1  = C';

    opinf2s.Cd = opinf1s.C(:,1:r);
    opinf2s.Cv = opinf1s.C(:,r+1:end);
end
end
