function opinf2_lcurve(x,y,r,Vr,v,a,t,u)   
% OPINF2_LCURVE saves the lcurve parameters    
    Nreg    = 20;
    epsilon = logspace(-6,-13,Nreg);   
    
    [ xp, vp, ap ] = data_proj(Vr,x,v,a);
    
    Data    = [-vp; -xp; u(t)];
    
   
    normerr = zeros(1,Nreg);
    normsolu = zeros(1,Nreg);
    for i = 1 : Nreg
        fprintf('Step %d \n',i);
        l = epsilon(i);
        opinf2s = opinf2(x,y,r,Vr,v,a,t, u, l);
        
        normsolu(i) = norm([opinf2s.D opinf2s.K opinf2s.B],'fro');
        normerr(i) = norm([opinf2s.D, opinf2s.K, opinf2s.B]*Data - ap, 'fro' );
    end
    save('lcurve_opinf.mat','normerr','normsolu','epsilon'); 
    
end