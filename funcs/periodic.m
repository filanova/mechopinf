function u = periodic(t)
    N = 2;
    om = 0.3;
    t0 = -0.1614;
    A0 = 0;
    B = [0.227, 0.413]; % coefficients
    A = zeros(1,2);
    u = 0;
    if t < t0
        u = A0;
    elseif t >= t0
        for i = 1:N
            u = u + A(i)*cos(i*om*(t - t0)) + B(i)*sin(i*om*(t-t0));
        end
        u = A0 + u;
    end
end