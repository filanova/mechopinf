function [a,v,d] = hht_amp(tspan,M,D,K,F,d0,v0)
%HHT_AMP Integration algorithm for second-order systems: Hilber-Hughes -
%Taylor family with negative alpha damping [1]
% In: [tspan] : time span
%        [M]  : mass system matrix
%        [D]  : damping system matrix
%        [K]  : stiffness system matrix
%        [F]  : external force matrix
%        [d0] : initial value vector for displacement vector
%        [v0] : initial value vector for velocity vector
%
% Out:   [a]  : acceleration vector
%        [v]  : velocity vector
%        [d]  : displacement vector
% ========================================================================

N = length(tspan);
h = tspan(2) - tspan(1);
k = size(M,1);

alpha = 0;                       % [-1/3; 0]
gamma = 0.5  * (1 - 2*alpha);    % guaranties unconditional stability
beta  = 0.25 * (gamma + 0.5)^2; 

b_ = 0.5 - beta;
g_ = 1 - gamma;
a_ = 1 + alpha;

d = zeros(k,N);
v = zeros(k,N);
a = zeros(k,N);

d(:,1) = d0;
v(:,1) = v0;
a(:,1) = M\(F(:,1) - K*d0 - D*v0);


Amp = M + a_*D*h*gamma + a_*K*h^2*beta;

for i = 1:N -1
    
    v_aux = v(:,i) + h*g_*a(:,i);
    d_aux = d(:,i) + h*v(:,i) + h^2*b_*a(:,i);
    
    DR = a_*D*v_aux - alpha*D*v(:,i);
    KR = a_*K*d_aux - alpha*K*d(:,i);
    
    R  = F(:, i+1)  - DR - KR;
   
    a(:,i+1) = Amp\R;
    d(:,i+1) = d_aux + h^2*beta*a(:,i+1);
    v(:,i+1) = v_aux + h*gamma*a(:,i+1);
end
end

% [1] Hilber, H.M, Hughes,T.J.R and Talor, R.L. "Improved Numerical 
% Dissipation for Time Integration Algorithms in Structural Dynamics" 
% Earthquake Engineering and Structural Dynamics, 5:282-292, 1977.

