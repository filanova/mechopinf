function [x_pod,x_opinf,x_opinfc,timef] = testing_rom(tend,h,u,opinf,opinfc,pod,r,Vr,Forcef )
%TESTING Simulation of the full order model, POD and operator inference 
%        reduced models
% Input: tend : end simulation time
%        h    : time step
%        u    : input signal function @(t)
%     {opinf} : ROM 2nd-order operator inference
%     {opinfc}: ROM 2nd-order force-informed operator inference
%     {pod}   : ROM POD 2nd-order
%
% Out: 
%      [x_pod]    : POD trajectories
%      [x_opinf]  : OpInf (unconstrained) trajectories
%      [x_opinfc] : OpInf (force-inforned) trajectories
%      [timef]    : time vector
% ========================================================================

Nf    = tend/h + 1;
timef = linspace(0,tend,Nf);

xr0 = zeros(r,1);

% POD
Fr = zeros(r,Nf);
for kk = 1:Nf
   Fr(:,kk) = pod.B*u(timef(kk));
end

[~,~,xr] = hht_amp(timef,pod.M,pod.D,pod.K,Fr,xr0,xr0);
x_pod = Vr*xr;

% OPINF without constraints
Frop = zeros(r,Nf);
for kk = 1:Nf
   Frop(:,kk) = opinf.B*u(timef(kk));
end
[~,~,xr_opinf] = hht_amp(timef,opinf.M,opinf.D,opinf.K,...
                                         Frop,xr0,xr0);
x_opinf = Vr*xr_opinf;

% OPINF with constraints
Fropc = Vr'*Forcef;
[~,~,xr_opinfc] = hht_amp(timef,opinfc.M,opinfc.D,opinfc.K,...
                                         Fropc,xr0,xr0);
x_opinfc = Vr*xr_opinfc;

end

