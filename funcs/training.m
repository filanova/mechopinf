function [a, v, x, y, Force, time] = training(tend,h,n,u,fom, out_vec)
% TRAINING Simulation of the given full order model (fom) 
%          with 2nd order integrator to collect the training data 
%
% Input: tend : end simulation time
%        h    : time step
%        n    : system dimension
%        u    : input signal function @(t)
%      {fom}  : structured array, including full-order matrices (M,K,D,B,C)
% out_vec = 1 : non-empty output vector | out_vec = 0 : empty output vector
%
% Out:   [a]  : acceleration vector
%        [v]  : velocity vector
%        [x]  : displacement vector
%        [y]  : output vector
%     [Force] : external force vector
%      [time] : time span
% ========================================================================

N    = round(tend/h) + 1;
time = linspace(0,tend,N);

Force  = zeros(n,N);
for kk = 1:N
   Force(:,kk) = fom.B*u(time(kk));
end

fprintf("Starting full-order model simulation\n Time step: %5.2f s\n End time: %d s\n",h,tend);

x0 = zeros(n,1);
v0 = x0;

[a,v,x] = hht_amp(time,fom.M,fom.D,fom.K,Force,x0,v0);

if out_vec
   y = fom.C1*[x; v];
else
    y = [];
end

end

