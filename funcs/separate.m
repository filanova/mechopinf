function [sep1,sep2,sep3] = separate(r,opinf2s)
%SEPARATE
%   Detailed explanation goes here
addpath('poldecomp');
% ________________________________________________________________________
% SEP 1
% ________________________________________________________________________

[Fi, om2, ~] = eig(opinf2s.K);

Fi = decomposition(Fi);

sep1.K = ((Fi')\om2)/Fi;
sep1.M = sep1.K/opinf2s.K;

sep1.D = sep1.M * opinf2s.D;
sep1.B = sep1.M * opinf2s.B;


% ________________________________________________________________________
% SEP 2: the nearest spd damping matrix
% ________________________________________________________________________

B = (sep1.D + sep1.D.')/2;

[~,H,~] = poldecomp(B);

sep2.D = (B + H)/2;
sep2.K = sep1.K;
sep2.M = sep1.M;
sep2.B = sep1.B;

% ________________________________________________________________________
% SEP 3: symm part of the mass&stiff matrix, nearest spd damp matrix
% ________________________________________________________________________
sep3.M = (sep2.M + sep2.M')/2;

sep3.D = sep3.M * opinf2s.D;

B = (sep3.D + sep3.D.')/2;
[~,H,~] = poldecomp(B);
sep3.D = (B + H)/2;

sep3.K = (sep2.K + sep2.K')/2;

sep3.B = sep3.M*opinf2s.B;
end

% opinf_sep2.C = opinf2s.C;
% 
% 
% opinf_sep1.A = [zeros(r),              eye(r); 
%                   -opinf_sep2.K, -opinf_sep2.D];
% opinf_sep1.B = [zeros(r,1); 
%                opinf_sep2.B];
% opinf_sep1.C  = opinf1s.C;