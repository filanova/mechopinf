function [ opinf2s ] = opinf_lsqlmi(r, a_pod, v_pod, x_pod, F_pod,lambda )
% OPINF_LSQLMI Least-squares optimization problem with LMI-constraints for
% 2nd-order operator inference method with known force vector.
% In: 
%           r    : reduced order
%       [x_pod]  : projected state trajectory matrix
%       [v_pod]  : projected velocity matrix
%       [a_pod]  : projected acceleration matrix
%       [F_pod]  : projected external force matrix 
%       lambda   : regularization parameter
%
% Out:
%     {opinf2s} - structured array with fields:
%                 [opinf2s.M]  :  mass matrix
%                 [opinf2s.K]  :  stiffness matrix
%                 [opinf2s.D]  :  damping matrix
%                 [opinf2s.B]  :  input matrix
%                 [opinf2s.Cd] :  position output matrix
%                 [opinf2s.Cv] :  velocity output matrix
% ========================================================================
addpath(genpath('../YALMIP/'));
addpath(genpath('../sedumi/')); %https://github.com/SQLP/SeDuMi

yalmip('clear');

% Variables
    Mr    = sdpvar(r,r); 
    Kr    = sdpvar(r,r);
    Dampr = sdpvar(r,r);

    Operators = [Mr; Dampr; Kr];
% Data matrix    
    Data      = [a_pod', v_pod', x_pod'];
    fprintf('Condition number of Data matrix %6.3e \n',cond(Data));
    
% Objective function    
    Obj = norm(Data*Operators - F_pod,'fro')^2 + lambda*norm(Operators,'fro')^2;

% Constraints
     om = 1e-10;
     CF = [Mr - om*eye(r,r) >=0, Dampr>=0, Kr - om*eye(r,r)>=0];

% Solving
    ops = sdpsettings('solver', 'sedumi','sedumi.eps', 1e-15,...
        'sedumi.cg.qprec', 1,'sedumi.cg.maxiter', 60,'sedumi.stepdif', 2);
    
    optimize(CF,Obj,ops);

    opinf2s.M = value(Mr');
    opinf2s.K = value(Kr');
    opinf2s.D = value(Dampr');

end
