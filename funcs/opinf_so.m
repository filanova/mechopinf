function [opinf2s] = opinf_so(x,y,r,Vr,v,a,tspan, u, lambda)
% OPINF2 Operator Inference reduction for second-order systems,
%        unconstrained
% In: 
%       [x]  : state trajectory matrix
%       [v]  : velocity matrix
%       [a]  : acceleration matrix
%       [y]  : output trajectory
%       [Vr] : projection basis
%    [tspan] : time span
%        u : input signal function @(t)
%   lambda : regularization parameter
%
% Out:
%     {opinf2s} - structured array with fields:
%                 [opinf2s.M]  :  mass matrix
%                 [opinf2s.K]  :  stiffness matrix
%                 [opinf2s.D]  :  damping matrix
%                 [opinf2s.B]  :  input matrix
%                 [opinf2s.Cd] :  position output matrix
%                 [opinf2s.Cv] :  velocity output matrix
% ========================================================================

    addpath(genpath('../YALMIP/'));
    addpath(genpath('../sedumi/'));
    
    yalmip('clear');
% Data projection    
    [ xp, vp, ap ] = data_proj(Vr,x,v,a);
    
    Data = [-vp', -xp', u(tspan)'];

% Variables 
    Kr    = sdpvar(r,r,'full');
    Dampr = sdpvar(r,r,'full');
    Br    = sdpvar(1,r);
   
    Operators = [Dampr; Kr; Br];

% Objective function
    Obj = norm(Data*Operators - ap','fro')^2 + lambda*norm(Operators,'fro')^2;
    
% Constraints
    CF = [Data*Operators == ap'];
%     CF = [];
% Solving    
    ops = sdpsettings('solver', 'sedumi','sedumi.eps', 1e-15,...
        'sedumi.cg.qprec', 1,'sedumi.cg.maxiter', 60,'sedumi.stepdif', 2);

    optimize(CF,Obj,ops);

    opinf2s.M = eye(r);
    opinf2s.K = transpose(value(Kr));
    opinf2s.D = transpose(value(Dampr));
    opinf2s.B = transpose(value(Br));

% Output-operator
if ~isempty(y)
    C = lsqminnorm([xp', vp'],y');
    C1  = C';
    opinf2s.Cd = C1(:,1:r);
    opinf2s.Cv = C1(:,r+1:end);
end
end
