function [ fom, n ] = assembly_iss()
% ASSEMBLY_ISS reads the mechanical system matrices for ISS benchmark
% Out: {fom} - structured array with fields:
%              [M] - mass matrix
%              [K] - stiffness matrix
%              [D] - damping matrix
%              [B] - input matrix
%              [C1] - position and velocity output matrix
%              name - string benchmark name
%       n - system dimension
% ========================================================================
load('iss.mat','A','B','C');
B_siso = B(:,1);
C_siso = C(1,:);
[M,K,Damp,B2,~,~] = sys2mechsys(A,B_siso,C_siso);
n = size(M,1);

fom.M = M;
fom.K = K;
fom.D = Damp;
fom.B = B2;
fom.C1 = C_siso;
fom.name = 'ISS';
end

