function opinf2c_lcurve(x,y,r,Vr,v,a, f)   
    Nreg = 20;
    epsilon = logspace(-6,-13,Nreg);
    [ xp, vp, ap, fp ] = data_proj(Vr,x,v,a,f);
    normerr = zeros(1,Nreg);
    normsolu = zeros(1,Nreg);
    for i = 1 : Nreg
        fprintf('Step %d \n',i);
        l = epsilon(i);
        opinf2s  = opinf2c(x,y,r,Vr,v,a, f, l);
        
        normsolu(i) = norm([opinf2s.M opinf2s.D opinf2s.K],'fro');
        normerr(i) = norm([opinf2s.M opinf2s.D opinf2s.K]*[ap; vp; xp] - fp, 'fro' );
    end
    save('lcurve_opinfc.mat','normerr','normsolu','epsilon');
    
end