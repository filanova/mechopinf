function [fom,ndof] = assembly_gyro()
% ASSEMBLY_GYRO reads the system matrices of butterfly gyroscope [1] example
% and returns the SISO system
% Out: fom  : structured array, including full-order matrices (M,K,D,B)

addpath(genpath('ButterflyGyro_dim1e5/'));

% Read matrices __________________________________________________________
M  = mmread('gyro.M');
K  = mmread('gyro.K');
B  = mmread('gyro.B');
C2 = mmread('gyro.C');

ndof = size(M,1);

% Make SISO ______________________________________________________________
C = C2(1,:);
B = B(:,1);

% Set the damping ________________________________________________________
alpha = 0;
beta  = 1e-6;
D     = alpha*M + beta*K;

fom.M = M;
fom.K = K;
fom.D = D;
fom.B = B;
fom.C2 = C;
fom.name = 'Gyroscope benchmark system matrices';

end
% [1] https://morwiki.mpi-magdeburg.mpg.de/morwiki/index.php/Butterfly_Gyroscope
