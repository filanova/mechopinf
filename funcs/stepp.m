function u = stepp(t)

if t <= 0.005
    u = 0.5;
elseif t > 0.005 && t <0.006
    u = -500*(t - 0.006);
elseif t >=0.006
    u = 0;
end
end

