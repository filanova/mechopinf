function r = ric(tol,S)
% Choose the reduced order for the model based on relative information
% content criterium
%
% INPUT:
%       tol - tolerance
%       [S] - Singular values matrix
    
    Sr     = diag(S);
    sum_sv = sum(Sr);

    for i = 1:length(Sr)
       RIC = sum(Sr(1:i))/sum_sv; 
       if (1 - RIC) < tol
           r = i;
           break;
       end
    end

end

