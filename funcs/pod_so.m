function [pod2s] = pod_so(Vr,M,K,Damp,B2,C,n)
% POD2 Proper Orthogonal Decomposition method for second-order systems
% In:
%       [Vr]   : projection basis
%       [M]    : FOM mass matrix
%       [K]    : FOM stiffness matrix 
%       [Damp] : FOM damping matrix
%       [B2]   : FOM input matrix
%       [C]    : FOM position and velocity output matrix 
%        n     : system dimension
%
% Out:
%       {pod2s} - structured array with fields:
%                 [pod2s.M]  : POD mass matrix
%                 [pod2s.K]  : POD stiffness matrix
%                 [pod2s.D]  : POD damping matrix
%                 [pod2s.B]  : POD input matrix
%                 [pod2s.Cd] : POD position output matrix
%                 [pod2s.Cv] : POD velocity output matrix
%                 [pod2s.C1] : POD output matrix
% ========================================================================

pod2s.M = Vr'*M*Vr;
pod2s.K = Vr'*K*Vr;
pod2s.B = Vr'*B2;
pod2s.D = Vr'*Damp*Vr;

if ~isempty(C)
    pod2s.Cd = C(:,1:n)*Vr;
    pod2s.Cv = C(:,n+1:end)*Vr;
    pod2s.C1  = [pod2s.Cd, pod2s.Cv];
end
end

