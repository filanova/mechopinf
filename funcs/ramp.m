function u = ramp(t)
    t_load = 0.5;
    t_pause = t_load + 0.5;
    
    F = 50;
    
     if t <= t_load        
         u = -F*t/t_load;
     elseif t > t_load && t <= t_pause
         u = -F;
     elseif t > t_pause
         u = F*(t - t_pause) - F;
     end
end


