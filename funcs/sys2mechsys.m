function [M,K,Damp,B2,Cp,Cv] = sys2mechsys(A,B,C)
% SYS2MECHSYS transforms the 1st-order ODE system into 2nd-order ODE system
%
%      E   \dot{z}    =        A         z     +  B   u
%
%    [I 0][\dot{x} ]  =   [ 0    I  ] [  x   ] + [0 ][u]
%    [0 I][\ddot{x}]      [-K -Damp ] [\dot{x}]  [B2]
%
%
%     y  =    C     z
%
%    [y] = [Cp Cv] [ x     ]
%                  [\dot{x}]
% ========================================================================

n    = size(A,1);            
k    =  n/2;                 
M    =  speye(k,k) ;
K    = -A(k+1:end,1:k);                 
Damp = -A(k+1:end,k+1:end);             
B2   =  B(k+1:end,:);                   
Cp   =  C(:,1:k);          % position output
Cv   =  C(:,k+1:end);      % velocity output

end

% if E == 1
%     M = speye(k,k);
% else
%     M    =  E(k+1:end,k+1:end);   
% end