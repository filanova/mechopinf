%% Operator Inference second-order formulation
% ========================================================================
% International Space Station benchmark [1]
% SISO, system dimension n = 270.         
% ========================================================================
clear all; close all; clc;

addpath(genpath('../../funcs/'));
addpath(genpath('../../YALMIP/'));
addpath(genpath('../../sedumi/'));
%  ________________________________________________________________________
%                            Parameters
%  ________________________________________________________________________
fig  = 1;            % 0 - no figures   ; 1 - plot and save figures
mat  = 0;            % 0 - no save      ; 1 - save .mat file
out_vec = 0;         % 0 - empty output vector; 1 - non-empty output vector

r = 4;               % reduced order

% Training
u    = @(t) sin(t);  % input signal
tend = 7;
h    = 1e-2;         % time step

% Testing
uf    = @(t) sin(t);
tendf = 21;
hf    = 1e-2;
%  ________________________________________________________________________
%                            Training data
% _________________________________________________________________________
[fom,n] = assembly_iss();

[a, v, x, y, f, t] = training(tend,h,n,u,fom,out_vec);
% ________________________________________________________________________
%                             Reduction
% ________________________________________________________________________
[U,S,~] = svd(x,'econ');
Vr = U(:,1:r);

% POD reduction
pod = pod_so(Vr,fom.M,fom.K,fom.D,fom.B,fom.C1,n);

% Type 1: OpInf unconstrained reduction 
opinf =  opinf_so(x,y,r,Vr,v,a,t, u, 0);

% Type 2: OpInf force-informed reduction
opinfc = opinf_so_f(x,y,r,Vr,v,a, f, 0);
% ________________________________________________________________________
%                               Testing
% ________________________________________________________________________
[x_hhtf,xh_pod,x_opinf,x_opinfc,timef] = testing(tendf,h,uf,fom,n,...
                                                 opinf,opinfc,pod,...
                                                 r,Vr,out_vec);
% ________________________________________________________________________
%                            Plot and save
% ________________________________________________________________________
if fig
    plot_iss;
end

if mat
   save('iss_opinf.mat','pod','opinf','opinfc','U','S','-v7.3');
end
% ________________________________________________________________________
%                            References
% ________________________________________________________________________
% [1] https://morwiki.mpi-magdeburg.mpg.de/morwiki/index.php/International_Space_Station
