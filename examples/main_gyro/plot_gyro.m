%% Plot the results from main_iss.m file
% Comparison of FOM and ROMs of order r = 6.
%  Figures:
%  1. Comparison of trajectories for specified degree of freedom (dof)
%     see Fig. 4 a)
%  2. Comparison of the relative errors
%     see Fig 4 b)
% ========================================================================
Nf = length(timef);
dof = 3143;

Color.OpInfc = [0.4940 0.1840 0.5560];
Color.OpInf  = [0.6350 0.0780 0.1840];
Color.POD    = [0.9290 0.6940 0.1250];

foldername = 'gyro_results';

% Plot DOF solution
figure;
p = plot(timef(1:10:end),x_hhtf(dof,1:10:end),...
         timef(1:30:end),xh_pod(dof,1:30:end),...
         timef(50:100:end),x_opinf(dof,50:100:end),...
         timef(10:45:end),x_opinfc(dof,10:45:end),...
         'LineWidth',1.3); 
grid on;
xlabel('t, s','Interpreter','LaTex');
ylabel('$x_{out}(t)$','Interpreter','LaTex');
p(2).Color =  Color.POD;
p(2).LineStyle = 'none';
p(2).Marker = '^';

p(3).Color =  Color.OpInf;
p(3).LineStyle = 'none';
p(3).Marker = 'o';
p(4).Color =  Color.OpInfc;
p(4).LineStyle = 'none';
p(4).Marker = 'diamond';
yticks([-1e-4 -0.5e-4 0 0.5e-4 1e-4])
hold on;
plot([0.001,0.001],[1e-4 -1e-4],'LineWidth',2,'Color','black');
legend({'FOM','POD','OpInf','cOpInf'},'FontSize',12);
hold off;
set(gcf,'Position',[500 400 1000 250])

mkdir(foldername);
print([foldername '/gyro_x'],'-depsc');
% X-Error vs time
reler_opinf_cd = zeros(Nf,1);
reler_pod_cd   = zeros(Nf,1);
reler_opinf = zeros(Nf,1);

normx = zeros(Nf,1);
    for i = 2 : Nf
        normx(i) = norm(x_hhtf(:,i));
        if normx(i)~=0
            reler_pod_cd(i)   = norm(x_hhtf(:,i) - xh_pod(:,i));%/normx(i);
            reler_opinf_cd(i) = norm(x_hhtf(:,i) - x_opinfc(:,i));%/normx(i);
            reler_opinf(i) = norm(x_hhtf(:,i) - x_opinf(:,i));%/normx(i);
        end
    end
reler_pod_cd = reler_pod_cd./max(normx);
reler_opinf_cd = reler_opinf_cd./max(normx);
reler_opinf = reler_opinf./max(normx);

figure;
p = semilogy(timef,reler_pod_cd,...
             timef,reler_opinf_cd,...
             timef,reler_opinf,...
             'LineWidth',1.3);   

grid on;
xlabel('t','Interpreter','LaTex');
ylabel('$\epsilon_{err}(t)$','Interpreter','LaTex');
p(1).Color =  Color.POD;
p(1).LineWidth = 1.3;
p(2).LineWidth = 1.3;
p(1).Marker = '^';
p(1).MarkerIndices = 1:60:length(reler_pod_cd);
p(2).Color =  Color.OpInfc;
p(2).Marker = 'diamond';
p(2).MarkerIndices = 1:50:length(reler_opinf_cd);
p(3).LineWidth = 1.3;
p(3).Color =  Color.OpInf;
p(3).Marker = 'o';
p(3).MarkerIndices = 1:50:length(reler_opinf);
hold on;
plot([0.001,0.001],[1e-4 -1e-4],'LineWidth',2,'Color','black');
legend({'POD', 'cOpInf','OpInf'},'FontSize',12);
hold off;
set(gcf,'Position',[500 400 1000 250])
mkdir(foldername);
print([foldername '/gyro_err_x'],'-depsc');