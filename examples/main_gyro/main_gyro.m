%% Operator Inference second-order formulation
% ========================================================================
% Butterfly Gyroscope benchmark [1]
% SISO, system dimension n = 17 361.       
% ========================================================================
clear all; close all; clc;

addpath(genpath('../../funcs/'));
addpath(genpath('../../YALMIP/'));
addpath(genpath('../../sedumi/'));
%  ________________________________________________________________________
%                            Parameters
%  ________________________________________________________________________
reg = 0; % 0 - run opinf with fixed reg.param.; 1 - reg. param. selection
fig = 1; % 0 - no figures   ; 1 - plot figures
mat = 0; % 0 - no save      ; 1 - save .mat file
out_vec = 0; % 0 - empty output vector; 1 - non-empty output vector

r    = 6; % reduced order

% Training
u    = @(t) sin(2*pi*1000*t); % input signal
tend = 0.001;
h    = 1e-6;                  % time step

% Testing
uf    = @(t) sin(2*pi*1000*t);
tendf = 0.003;
hf     = 1e-6;
%  ________________________________________________________________________
%                           Training data
% _________________________________________________________________________
[fom,n] = assembly_gyro();

[a, v, x, y, f, t] = training(tend,h,n,u,fom,out_vec);
% ________________________________________________________________________
%                             Reduction
% ________________________________________________________________________
[U,S,~] = svd(x,'econ');
Vr = U(:,1:r);

% POD reduction
pod = pod_so(Vr,fom.M,fom.K,fom.D,fom.B,[fom.C2, sparse(1,n)],n);

if reg
    % Type 1: OpInf unconstrained reduction 
    opinf2_lcurve(x,y,r,Vr,v,a,t, u);
    
    % Type 2: OpInf force-informed reduction
    opinf2c_lcurve(x,y,r,Vr,v,a, f);
else
    lambda_1 = 7e-10;
    lambda_2 = 4.8e-10;
    % Type 1: OpInf unconstrained reduction 
    opinf =  opinf_so(x,y,r,Vr,v,a,t, u, lambda_1);

    % Type 2: OpInf force-informed reduction
    opinfc = opinf_so_f(x,y,r,Vr,v,a, f, lambda_2);
end
% ________________________________________________________________________
%                               Testing
% ________________________________________________________________________
[x_hhtf,xh_pod,x_opinf,x_opinfc,timef] = testing(tendf,h,uf,fom,n,...
                                                 opinf,opinfc,pod,r,Vr,out_vec);
% ________________________________________________________________________
%                            Plot and save
% ________________________________________________________________________

if fig
    plot_gyro;
end

if mat
   save('gyro_opinf.mat','pod','opinf','opinfc','U','S','-v7.3');
end

% [1] https://morwiki.mpi-magdeburg.mpg.de/morwiki/index.php/Butterfly_Gyroscope
