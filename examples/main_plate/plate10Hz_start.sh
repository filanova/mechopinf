#!/bin/bash
# Your job name.
#SBATCH -J plate10Hz
#
# Output files. .out. is for standard output .err. is for the error output.
#SBATCH -o %x-%j.out-%N
#SBATCH -e %x-%j.err-%N
#
# Maximum expected runtime.  ( 10 Days, 0 hour, 00 minutes, 00 seconds)
#SBATCH --time=10-0:00:00
#
# Allocate one node with all 16 CPU cores
#SBATCH --nodes=1
#SBATCH --cpus-per-task=16
#
# Choose Partition (Queue)
#SBATCH --partition gpu
#
# Choose Constraints (node type, here restricting to standard nodes)
#SBATCH --constraint=RAM192
#
# Mail Options
#SBATCH --mail-type=FAIL,BEGIN,END    # An email is sent on begin, end, and failure of the job
#SBATCH --mail-user=filanova@mpi-magdeburg.mpg.de   # E-Mail for notification
#
### END OF THE SLURM SPECIFIC PART ###

# Setup OpenMP
if [ -n "$SLURM_CPUS_PER_TASK" ]; then
  omp_threads=$SLURM_CPUS_PER_TASK
else
  omp_threads=1
fi
export OMP_NUM_THREADS=$omp_threads

# set up the environment (choose whats needed)
# load the modules system
source /etc/profile.d/modules.sh


# Load MATLAB

module load apps/matlab/2021a

# Or one of the older versions
#module load apps/matlab/2012b
#module load apps/matlab/2016b


# SETUP Your MATLAB environment 
# The default search path for m-files.
export MATLABPATH=.


#run your script
matlab -nojvm -nosplash -batch "main_plate;" < /dev/null
