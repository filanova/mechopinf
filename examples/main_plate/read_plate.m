function [fom,n] = read_plate()
%READ_PLATE Assembly system matrices of vibrationg plate example
%   Detailed explanation goes here

load('plate_48_rayleigh_single.mat');

fom.M = A{3};
fom.K = A{1};
fom.D = A{2};
fom.B = b;
n = size(fom.M,1);
fom.C1 = [transpose(c), sparse(1,n)];
fom.name = 'Vibrating Plate';


end

