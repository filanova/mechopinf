%% Plot the results from main_iss.m file
% Comparison of FOM and ROMs of order r = 110.
%  Figures:
%  1. Comparison of trajectories for specified degree of freedom (dof)
%     see Fig. 6 a)
%  2. Comparison of the relative errors
%     see Fig 6 b)
% ========================================================================
Nf = length(timef);
dof = 176544;


Color.OpInfc = [0.4940 0.1840 0.5560];
Color.OpInf  = [0.6350 0.0780 0.1840];
Color.POD    = [0.9290 0.6940 0.1250];

foldername = 'plate_results';
%  ________________________________________________________________________
%                         Plot solution trajectories
% _________________________________________________________________________
fig = figure('visible','off');
p = plot(timef(1:10:end),x_hhtf(dof,1:10:end),...
         timef(1:20:end),xh_pod(dof,1:20:end),...
         timef(10:45:end),x_opinfc(dof,10:45:end),...
         'LineWidth',1.3); 
grid on;
xlabel('t, s','Interpreter','LaTex');
ylabel('$x_{out}(t)$','Interpreter','LaTex');
p(2).Color =  Color.POD;
p(2).LineStyle = 'none';
p(2).Marker = '^';
p(3).Color =  Color.OpInfc;
p(3).LineStyle = 'none';
p(3).Marker = 'diamond';
hold on;
plot([0.5,0.5],[1e-5 -1e-5],'LineWidth',2,'Color','black');
legend({'FOM','POD','cOpInf'},'FontSize',12);
hold off;
set(gcf,'Position',[500 400 1000 250])

mkdir(foldername);
saveas(fig,[foldername '/plate_x.eps']);
close(fig); 
%  ________________________________________________________________________
%                      Plot relative error vs time
% _________________________________________________________________________
reler_opinf_cd = zeros(Nf,1);
reler_pod_cd   = zeros(Nf,1);
reler_opinf = zeros(Nf,1);

normx = zeros(Nf,1);
    for i = 2 : Nf
        normx(i) = norm(x_hhtf(:,i));
        if normx(i)~=0
            reler_pod_cd(i)   = norm(x_hhtf(:,i) - xh_pod(:,i));
            reler_opinf_cd(i) = norm(x_hhtf(:,i) - x_opinfc(:,i));
            reler_opinf(i) = norm(x_hhtf(:,i) - x_opinf(:,i));
        end
    end
reler_pod_cd = reler_pod_cd./max(normx);
reler_opinf_cd = reler_opinf_cd./max(normx);
reler_opinf = reler_opinf./max(normx);

fig = figure('visible','off');
p = semilogy(timef,reler_pod_cd,...
             timef,reler_opinf_cd,...
             timef,reler_opinf,...
             'LineWidth',1.3);   
grid on;
xlabel('t','Interpreter','LaTex');
ylabel('$\epsilon_{err}(t)$','Interpreter','LaTex');
p(1).Color =  Color.POD;
p(1).LineWidth = 1.3;
p(2).LineWidth = 1.3;
p(1).Marker = '^';
p(1).MarkerIndices = 1:70:length(reler_pod_cd);
p(2).Color =  Color.OpInfc;
p(2).Marker = 'diamond';
p(2).MarkerIndices = 1:50:length(reler_opinf_cd);
p(3).LineWidth = 1.3;
p(3).Color =  Color.OpInf;
p(3).Marker = 'o';
p(3).MarkerIndices = 1:50:length(reler_opinf);
hold on;
plot([0.5,0.5],[1e-5 -1e-5],'LineWidth',2,'Color','black');
legend({'POD', 'cOpInf','OpInf'},'FontSize',12);
hold off;
saveas(fig,[foldername '/plate_err_x.eps']);
close(fig);
