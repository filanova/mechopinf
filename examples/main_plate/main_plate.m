%% Operator Inference second-order formulation
% ========================================================================
% Vibrating plate [1]
% SISO, system dimension n = 201 900.       
% ========================================================================
clear all; close all; clc;

addpath(genpath('../../funcs/'));
addpath(genpath('../../YALMIP/'));
addpath(genpath('../../sedumi/'));
%  ________________________________________________________________________
%                            Parameters
%  ________________________________________________________________________
reg = 0; % 0 - run opinf with fixed reg.param.; 1 - reg. param. selection
fig = 1; % 0 - no figures   ; 1 - save figures
mat = 0; % 0 - no save      ; 1 - save .mat file
out_vec = 0; % 0 - empty output vector; 1 - non-empty output vector

r    = 110; % reduced order


% Training
u    = @(t) sin(2*pi*10*t); % input signal
tend = 0.5;
h    = 1e-3;        % time step

% Testing
uf    = @(t) sin(2*pi*10*t); % @(t) periodic(t); @(t) ramp(t);
tendf = 1;
hf     = 1e-3;
%  ________________________________________________________________________
%                           Training data
% _________________________________________________________________________
[fom,n] = read_plate();

[a_test, v_test, x_test, y_test, f_test, t_test] = training(tendf,hf,n,uf,fom,out_vec);
a = a_test(:,1:501);
v = v_test(:,1:501);
x = x_test(:,1:501);

f = f_test(:,1:501);
t = t_test(1:501);

if out_vec
    y = y_test(1:501);
else
    y = [];
end

clear a_test v_test y_test t_test
x_hhtf = x_test;
clear x_test
save('plate10Hz_data.mat','a','v','x','f','t','y','x_hhtf','-v7.3');
% ________________________________________________________________________
%                             Reduction
% ________________________________________________________________________
[U,S,~] = svd(x,'econ');
Vr = U(:,1:r);

% POD reduction
pod = pod_so(Vr,fom.M,fom.K,fom.D,fom.B,fom.C1,n);

if reg
    % Type 1: OpInf unconstrained reduction 
    opinf2_lcurve(x,y,r,Vr,v,a,t, u);
    
    % Type 2: OpInf force-informed reduction
    opinf2c_lcurve(x,y,r,Vr,v,a, f);
else
    lambda_1 = 0;
    lambda_2 = 0;
    % Type 1: OpInf unconstrained reduction 
    opinf =  opinf_so(x,y,r,Vr,v,a,t, u, lambda_1);

    % Type 2: OpInf force-informed reduction
    opinfc = opinf_so_f(x,y,r,Vr,v,a, f, lambda_2);
end
% ________________________________________________________________________
%                               Testing
% ________________________________________________________________________
[xh_pod,x_opinf,x_opinfc,timef] = testing_rom(tendf,hf,uf,opinf,opinfc,pod,r,Vr,f_test );
% ________________________________________________________________________
%                            Plot and save
% ________________________________________________________________________

if fig
    plot_plate;
end

if mat
   save('plate_rom_res.mat','xh_pod','x_opinf','x_opinfc','timef','-v7.3');
   save('plate_rom.mat','pod','opinf','opinfc','U','S','-v7.3');
end

% [1] https://zenodo.org/record/5836047#.Y4ibY2HMLJk
