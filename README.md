# Operator Inference method for Mechanical Systems (MechOpInf)

This repository contains the Matlab implementation of operator inference methodology for **second-order ODE systems** [1] of type:

```math
 M \ddot{\mathbf{x}}(t) + C \dot{\mathbf{x}}(t) + K \mathbf{x}(t) = B \mathbf{u}(t)
 ```
where <br>
$` M \succ 0 `$  - mass matrix, $` \; C \succeq 0 `$ - damping matrix, $` \; K \succ 0 `$ - stiffness matrix, $` \; B `$ - input matrix;<br>
$`\mathbf{x} `$ - state vector, $` \; \mathbf{u} `$ - input signal vector.<br>

Assuming that the high-fidelity system realization $` (M,C,K,B) `$ of dimension $` n `$ is unknown, the goal is to find the reduced system representation $` (\widehat{M},\widehat{C},\widehat{K},\widehat{B}) `$ from available data (e.g. from state $` \mathbf{x} `$, inputs $` \mathbf{u} `$, etc.). The main idea is to infer the reduced operators solving the least-squares problem for low-dimensional data representation.

The operator inference methodology was originally developed in [3] for first-order ODE systems. Another extension for second-order ODE systems can be found in [2].

We consider two cases:

1. Obtaining the reduced model from given $` \mathbf{x}(t), \dot{\mathbf{x}}(t), \ddot{\mathbf{x}}(t)`$; ([``opinf_so.m``](/funcs/opinf_so_f.m) function)
2. Obtaining the reduced model from given $` \mathbf{x}(t), \dot{\mathbf{x}}(t), \ddot{\mathbf{x}}(t), \mathbf{f} (t) = B u(t)`$ ([``opinf_so_f.m``](/funcs/opinf_so_f.m) function)

## Structure

This repository contains the following folders:<br>

``funcs``   :  includes all the relevant operator inference matlab-functions<br>
``examples``:  includes the numerical examples according to [1].

## Code

The computations were originally performed in MATLAB (R2021a).

Additional software packages need to be downloaded:

* [YALMIP Toolbox](https://yalmip.github.io/) [4]
* [SEDUMI solver](https://sedumi.ie.lehigh.edu/) [5]

## Start instructions

In order to compute the ROMs and create the comparison plots according to [1]:
1. go to the [``examples``](/examples) folder;
2. choose the folder with the benchmark of interest (e.g. [``main_gyro``](/examples/main_gyro/) for Butterfly Gyroscope example);
3. start the file ``main_example-name.m``
   (optionally change the settings inside the ``main_example-name.m`` regarding plotting and saving results).

The plots will be saved in the folder ``example-name_results`` in the .eps format.

## References
[1] Filanova, Y., Pontes, I., Goyal, P. and Benner, P. [An Operator Inference Oriented Approach for Mechanical Systems](https://arxiv.org/abs/2210.07710v1), arXiv, 2022.<br>

[2] Sharma, H. and Kramer, B., [Preserving Lagrangian structure in data-driven reduced-order modeling of large-scale mechanical systems](https://arxiv.org/abs/2203.06361), arXiv, 2022.<br>

[3] Peherstorfer, B. and Willcox, K., [Data-driven operator inference for non-intrusive projection-based model reduction](https://www.sciencedirect.com/science/article/pii/S0045782516301104 ). Computer Methods in Applied Mechanics and Engineering, Vol. 306, pp. 196-215, 2016.<br>

[4] Löfberg J., [YALMIP : A Toolbox for Modeling and Optimization in MATLAB](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=1393890). In Proceedings of the CACSD Conference, 2004.<br>

[5] Sturm, J.F., [Using SeDuMi 1.02, A Matlab toolbox for optimization over symmetric cones](https://www.tandfonline.com/doi/pdf/10.1080/10556789908805766?needAccess=true), Optimization Methods and Software, 11:1-4, 625-653.

